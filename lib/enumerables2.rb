require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(0) { |acc, el| acc + el}
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.each do |string|
    return false if sub_string?(string, substring) == false
  end
  true
end

def sub_string?(long_string, sub_string)
  long_string.include?(sub_string)
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  non_uniq = string.delete(" .?!;:").split("").select do |char| string.count(char) > 1
  end
  non_uniq.sort.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  arr = string.split(" ").sort_by do |word| word.length
  end
  arr[-2..-1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  str = string.split("")
  ("a".."z").reject do |char| str.include?(char)
  end
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select do |year| not_repeat_year?(year)
  end
end

def not_repeat_year?(year)
  y = year.to_s.split("")
  y.each_with_index do |first_digit, idx|
    y[idx + 1...y.length].each do |second_digit|
      return false if first_digit == second_digit
    end
  end
  true
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  songs.select {|song| no_repeats?(song, songs)}.uniq
end

def no_repeats?(song_name, songs)
  songs.each_with_index do |hit, idx|
    if hit == song_name
      return false if hit == songs[idx + 1]
    end
  end
  true
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string.delete(".,!?;:").split(" ").select { |word| word.include?("c") }.sort.first
end

def c_distance(word)
  word.split("").each_with_index do |char, idx|
    if char == "c"
      dist = word.length - idx
    end
  end
  dist
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result = []
  j = 0
  while j < arr.length
    if arr[j] == arr[j + 1]
      i = 1
      while arr[j + i] == arr[j]
        i += 1
      end
      result << [j, j + i - 1]
      j += i
    else
      j += 1
    end
  end
  result
end
